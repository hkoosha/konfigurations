/**
 * This is an internal implementation. To get an instance, use
 * {@link io.koosha.konfiguration.KonfigurationFactory#getInstance()}
 *
 * This package hides all implementing classes, and only exposes the
 * {@link io.koosha.konfiguration.impl.v8.FactoryV8}, which yet, shouldn't
 * be accessed directly.
 */
@ApiStatus.Internal
package io.koosha.konfiguration.impl.v8;

import org.jetbrains.annotations.ApiStatus;